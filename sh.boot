#!/bin/bash

app="jokeish"
path="."
case $1 in
    start)  # 服务启动需要做的步骤
        cd ${path}
        echo "" > nohup.out
        nohup ${path}/${app}  &
        ;;
    stop)  # 服务停止需要做的步骤
        cd ${path}
        kill -2 `cat ${path}/this.pid`
        rm ${path}/this.pid
        ;;
#!/bin/bash
    restart) # 重启服务需要做的步骤
        cd ${path}
        kill -2 `cat ${path}/this.pid`
       # sleep 5
        nohup ${path}/${app} &
        ;;
    status) # 查看状态需要做的步骤
        echo "sorry, I do not know"
        ;;
    *) echo "$0 {start|stop|restart|status}"
        exit 4
        ;;
esac
